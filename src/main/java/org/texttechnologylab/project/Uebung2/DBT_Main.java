package org.texttechnologylab.project.Uebung2;

import org.apache.commons.cli.*;
import org.texttechnologylab.project.Uebung2.data.*;
import org.texttechnologylab.project.Uebung2.data.database.MongoDBConfig;
import org.texttechnologylab.project.Uebung2.data.database.MongoDBConnectionHandler;
import org.texttechnologylab.project.Uebung2.data.exceptions.ParamException;
import org.texttechnologylab.project.Uebung2.data.impl.ParliamentFactory_Impl;
import org.texttechnologylab.project.Uebung2.data.impl.PlenaryProtocol_File_Impl;
import org.texttechnologylab.project.Uebung2.data.impl.Speaker_Plain_File_Impl;
import org.texttechnologylab.project.Uebung2.data.exceptions.InputException;
import org.texttechnologylab.project.Uebung2.helper.Reader;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.*;

public class DBT_Main {

        private ParliamentFactory pFactory = null;
        private MongoDBConnectionHandler dbhandler = null;

        public static void main(String[] args) throws ParseException, IOException, SAXException {
                Options options = new Options();

                options.addOption("db", true, "path to db_config_file");

                CommandLineParser parser = new DefaultParser();
                CommandLine cmd = parser.parse(options, args);
                MongoDBConnectionHandler dbHandler;

                try {
                        if (cmd.hasOption("db")) {
                                String dbConfigPath = cmd.getOptionValue("db");
                                MongoDBConfig dbConfig = new MongoDBConfig(dbConfigPath);
                                dbHandler = new MongoDBConnectionHandler(dbConfig);
                        } else {
                                throw new ParamException("no option -f defined");
                        }
                        DBT_Main dbt = new DBT_Main(dbHandler);
                        dbt.start();
                }
                catch (ParamException e) {
                        System.out.println("Beispiel Aufruf: -db C:\\Users\\Folder\\DBConfig.txt");
                }
        }
        public DBT_Main(MongoDBConnectionHandler dbHandler) {
                this.pFactory = new ParliamentFactory_Impl();
                Scanner scan = new Scanner(System.in);
                System.out.println("INSERT PATH TO XML_FILES");
                String path = scan.nextLine();
                this.read(path);
                System.out.println("would you like to drop all previous Documents?\n"+
                        "y\n"+
                        "n");
                path = scan.nextLine();
                if (Objects.equals(path, "y")) {
                        dbHandler.deleteAll();
                }
                dbHandler.insertNames(pFactory);
                dbHandler.insertNamesPartei(pFactory);
        }
        public DBT_Main() {
                this.pFactory = new ParliamentFactory_Impl();
        }

        public void start() throws InputMismatchException{

                Scanner scanner = new Scanner(System.in);

                System.out.println("MOINSEN");

                String input;

                while (true) {

                        try {

                                System.out.println("\nSelect an option by inputting an Integer: \n" +
                                        "1: Auflistung aller Redner\n" +
                                        "2: Auflistung eines bestimmten Abgeordneten\n" +
                                        "3: Auflistung aller Redner nach Partei/Fraktion\n" +
                                        "4: Ausgabe des Textes eines Tagesordnungspunktes gemäßt Situngs- und Nummern-Index\n" +
                                        "5: exit\n" +
                                        "\n" +
                                        "New Feature: \n" +
                                        "6: Datenbank ansehen");

                                input = scanner.nextLine();

                                switch (input) {

                                        case "1":
                                                this.getFactory().getSpeakers().stream().filter(s -> {
                                                        return !(s instanceof Speaker_Plain_File_Impl);
                                                }).sorted(Comparator.comparing(s -> s.getName().toLowerCase())).forEach(System.out::println);
                                                break;

                                        case "2":

                                                System.out.println("\nBitte Vor- oder Nachnamen angeben");
                                                String speaker = scanner.next();
                                                this.getFactory().getSpeakers().stream().filter(s -> {
                                                        return s.toString().toLowerCase().contains(speaker.toLowerCase());
                                                }).forEach(s -> {
                                                        System.out.println(s);
                                                });
                                                break;

                                        case "3":
                                                System.out.println("\nnach Partei oder Fraktion filtern?\n" +
                                                        "1: Partei\n" +
                                                        "2: Fraktion");

                                                String select = scanner.nextLine();

                                                switch (select) {

                                                        case "1":
                                                                AtomicInteger i = new AtomicInteger(1);
                                                                this.getFactory().getParties().stream().forEach(p -> {
                                                                        System.out.println("\t (" + (i.getAndIncrement()) + ") " + p.getName() + "gg");
                                                                });

                                                                int pp = Integer.valueOf(scanner.next());

                                                                Party p = getFactory().getParties().stream().collect(Collectors.toList()).get(pp - 1);
                                                                System.out.println("Partei: " + p.getName());
                                                                p.getMembers().stream().forEach(Member -> {
                                                                        System.out.println(Member.getName());
                                                                });
                                                                break;

                                                        case "2":
                                                                AtomicInteger iF = new AtomicInteger(1);
                                                                this.getFactory().getFractions().stream().forEach(pFraction -> {
                                                                        System.out.println("\t (" + (iF.getAndIncrement()) + ") " + pFraction.getName());
                                                                });

                                                                int ppp = Integer.valueOf(scanner.next());

                                                                Fraction pFraction = this.getFactory().getFractions().stream().collect(Collectors.toList()).get(ppp - 1);
                                                                System.out.println("Fraction: " + pFraction.getName());
                                                                pFraction.getMembers().stream().forEach(pMember -> {
                                                                        System.out.println(pMember);
                                                                });
                                                                break;

                                                        default:
                                                                throw new InputException("input an listed Integer please instead of: " + select);
                                                }
                                                break;

                                        case "5":
                                                System.out.println("Tschüssiii");
                                                System.exit(0);
                                        case "6":
                                                System.out.println("Oder man kann es doch nicht ansehen :/");
                                                break;
                                        default:
                                                throw new InputException("input an listed Integer please instead of: " + input);
                                }
                        }
                        catch (InputMismatchException | InputException e) {
                                e.printStackTrace();
                        }
                }
        }

        public ParliamentFactory getFactory() {
                return this.pFactory;
        }

        public void read(String path) {

                Reader.getFiles(path, "xml").stream().forEach(file -> {
                        PlenaryProtocol pProtocol = new PlenaryProtocol_File_Impl(this.pFactory, file);
                        getFactory().addProtocol(pProtocol);
                });
        }
}
