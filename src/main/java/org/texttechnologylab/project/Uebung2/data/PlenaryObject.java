package org.texttechnologylab.project.Uebung2.data;

public interface PlenaryObject {
    String getID();

    void setID(String lID);

    int getWahlperiode();

    void setWahlperiode(int iValue);

    ParliamentFactory getFactory();

    int compareTo(PlenaryObject plenaryObject);

    @Override
    boolean equals(Object o);

    @Override
    int hashCode();
}
