package org.texttechnologylab.project.Uebung2.data.impl;

import org.texttechnologylab.project.Uebung2.data.*;
import org.w3c.dom.Node;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of the ParliamantFactory for access to the individual components
 * @author Giuseppe Abrami
 */
public class ParliamentFactory_Impl implements ParliamentFactory {

    // Variables for storing the protocol informations
    private Set<Speaker> pSpeaker = new HashSet<>();
    private Set<PlenaryProtocol> pProtocols = new HashSet<>();
    private Set<Fraction> pFractions = new HashSet<>();
    private Set<Party> pParties = new HashSet<>();

    /**
     * Default constructor
     */
    public ParliamentFactory_Impl(){

    }

    @Override
    public Set<Speaker> getSpeakers(){
        return this.pSpeaker;
    }

    @Override
    public Set<PlenaryProtocol> getProtocols(){
        return this.pProtocols;
    }

    @Override
    public void addProtocol(PlenaryProtocol pProtocol) {
        this.pProtocols.add(pProtocol);
    }

    @Override
    public Set<Fraction> getFractions(){
        return pFractions;
    }

    @Override
    public Set<Party> getParties(){
        return this.pParties;
    }

    @Override
    public Party getParty(String sName) {
        List<Party> sList = this.getParties().stream().filter(s->s.getName().equalsIgnoreCase(sName)).collect(Collectors.toList());

        if(sList.size()==1){
            return sList.get(0);
        }
        else{
            Party pParty = new Party_File_Impl(sName);
            this.pParties.add(pParty);
            return pParty;
        }

    }

    /**
     * @param sId
     * @return
     */
    @Override
    public Speaker getSpeaker(String sId) {

        List<Speaker> sList = this.getSpeakers().stream().filter(s->s.getID().equals(sId)).collect(Collectors.toList());

        if(sList.size()==1){
            return sList.get(0);
        }

        return null;

    }

    public Speaker getSpeakerByName(String sValue){

        List<Speaker> sList = this.getSpeakers().stream().filter(s->{
            return s.getName().equalsIgnoreCase(Speaker_Plain_File_Impl.transform(sValue));
        }).collect(Collectors.toList());

        if(sList.size()==1){
            return sList.get(0);
        }
        return null;

    }

    @Override
    public Speaker getSpeaker(Node pNode) {

        Speaker pSpeaker = null;

        // if speaker is a complex node
        if(!pNode.getNodeName().equalsIgnoreCase("name")){
            String sID = pNode.getAttributes().getNamedItem("id").getTextContent();

            pSpeaker= getSpeaker(sID);

            if(pSpeaker==null){
                Speaker_File_Impl nSpeaker = new Speaker_File_Impl(this, pNode);
                this.pSpeaker.add(nSpeaker);
                pSpeaker = nSpeaker;
            }
        }
        // if not...
        else{
            pSpeaker = getSpeakerByName(pNode.getTextContent());

            if(pSpeaker==null){
                Speaker_Plain_File_Impl plainSpeaker = new Speaker_Plain_File_Impl(this);
                plainSpeaker.setName(pNode.getTextContent());

                this.pSpeaker.add(plainSpeaker);
                pSpeaker = plainSpeaker;
            }

        }

        return pSpeaker;
    }

    @Override
    public Fraction getFraction(String sName) {
        List<Fraction> sList = this.getFractions().stream().filter(s->{
            if(s.getName().startsWith(sName.substring(0, 3))){
                return true;
            }
            return s.getName().equalsIgnoreCase(sName.trim());
        }).collect(Collectors.toList());

        if(sList.size()==1){
            return sList.get(0);
        }

        return null;
    }

    @Override
    public Fraction getFraction(Node pNode) {
        String sName = pNode.getTextContent();

        Fraction pFraction = getFraction(sName);

        if(pFraction==null){
            // if fraction not exist, create
            pFraction = new Fraction_File_Impl(pNode);
            this.pFractions.add(pFraction);
        }

        return pFraction;
    }
}
