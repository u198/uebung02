package org.texttechnologylab.project.Uebung2.data.impl;

import org.texttechnologylab.project.Uebung2.data.Speaker;
import org.texttechnologylab.project.Uebung2.data.Speech;
import org.texttechnologylab.project.Uebung2.data.Text;

public class Text_File_Impl implements Text {

    private Speaker pSpeaker = null;
    private Speech pSpeech = null;
    private String sText = "";

    /**
     * Constructor
     * @param pSpeaker
     * @param pSpeech
     * @param sText
     */
    public Text_File_Impl(Speaker pSpeaker, Speech pSpeech, String sText){

        this.pSpeaker = pSpeaker;
        this.pSpeech = pSpeech;
        this.sText = sText;

    }

    public Text_File_Impl(String sText){
        this.sText = sText;
    }

    @Override
    public Speech getSpeech() {
        return this.pSpeech;
    }

    @Override
    public Speaker getSpeaker() {
        return this.pSpeaker;
    }

    @Override
    public void setSpeech(Speech pSpeech) {
        this.setSpeech(pSpeech);
    }

    @Override
    public void setSpeaker(Speaker pSpeaker) {
        this.setSpeaker(pSpeaker);
    }

    @Override
    public String getContent() {
        return this.sText;
    }

}
