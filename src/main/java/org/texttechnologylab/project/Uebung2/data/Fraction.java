package org.texttechnologylab.project.Uebung2.data;

import java.util.Set;

public interface Fraction extends Comparable<Fraction>{
    String getName();

    void addMember(Speaker pSpeaker);

    Set<Speaker> getMembers();

    int compareTo(Fraction fraction);

    @Override
    boolean equals(Object o);

    @Override
    int hashCode();

    @Override
    String toString();
}
