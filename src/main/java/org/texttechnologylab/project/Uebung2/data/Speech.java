package org.texttechnologylab.project.Uebung2.data;

import java.util.List;

public interface Speech extends PlenaryObject {
    AgendaItem getAgendaItem();

    List<Comment> getComments();

    String getText();

    PlenaryProtocol getProtocol();

    Speaker getSpeaker();

    void setSpeaker(Speaker pSpeaker);

    int getLength();

    List<Speech> getInsertions();

    void addText(Text pText);

    @Override
    String toString();
}
