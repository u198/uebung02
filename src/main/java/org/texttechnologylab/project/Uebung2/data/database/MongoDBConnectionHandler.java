package org.texttechnologylab.project.Uebung2.data.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.texttechnologylab.project.Uebung2.data.ParliamentFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MongoDBConnectionHandler {

    private MongoDBConfig pConfig = null;

    private MongoClient pClient = null;

    private MongoDatabase pDatabase = null;

    private MongoCollection<Document> pCollection = null;

    private MongoCollection<Document> pCollection2 = null;

    public MongoDBConnectionHandler(MongoDBConfig pConfig) {
        this.pConfig = pConfig;
        init();
    }

    public MongoDBConnectionHandler() {

    }

    private void init() {
        // define credentials (Username, database, password)
        MongoCredential credential = MongoCredential.createScramSha1Credential(pConfig.getMongoUsername(), pConfig.getMongoDatabase(), pConfig.getMongoPassword().toCharArray());

        // defining Hostname and Port
        ServerAddress seed = new ServerAddress(pConfig.getMongoHostName(), pConfig.getMongoPort());
        List<ServerAddress> seeds = new ArrayList(0);
        seeds.add(seed);

        // defining some Options
        MongoClientOptions options = MongoClientOptions.builder()
                .connectionsPerHost(10)
                .socketTimeout(5000)
                .maxWaitTime(1000)
                .connectTimeout(1000)
                .sslEnabled(false)
                .build();

        // connect to MongoDB
        pClient = new MongoClient(seeds, credential, options);

        // select database
        pDatabase = pClient.getDatabase(pConfig.getMongoDatabase());
        // select default collection based on the config
        pCollection = pDatabase.getCollection(pConfig.getMongoCollection());
        pCollection2 = pDatabase.getCollection(pConfig.getMongoCollectionP());


        // some debug information
        System.out.println("Connect to " + pConfig.getMongoDatabase() + " on " + pConfig.getMongoHostName());
    }

    public void insertNames(ParliamentFactory pF) {
        pF.getSpeakers().stream().forEach(s -> {
            Document doc = new Document("name", s.getName() + " " + s.getFirstName())
                    .append("Title", s.getTitle())
                    .append("Role", s.getRole())
                    //.append("Fraktion", s.getFraction())
                    .append("Party", s.getParty());
            List<Document> list = new ArrayList<>();
            list.add(doc);
            pCollection.insertMany(list);
        });
        FindIterable<Document> iterdoc = pCollection.find();
        int i = 1;
        Iterator<Document> it = iterdoc.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
            i++;
        }
    }
    public void insertNamesPartei(ParliamentFactory pF) {
        pF.getFractions().stream().forEach(s -> {
            String wowsy= s.getMembers().toString();
            Document doc = new Document("Partei", s.getName())
                    .append("name", wowsy);
            List<Document> list = new ArrayList<>();
            list.add(doc);
            pCollection2.insertMany(list);
        });
        FindIterable<Document> iterdoc = pCollection2.find();
        int i = 1;
        Iterator<Document> it = iterdoc.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
            i++;
        }
    }
    public void deleteAll(){
        pCollection.drop();
        pCollection2.drop();
    }
}
