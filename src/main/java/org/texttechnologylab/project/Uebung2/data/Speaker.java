package org.texttechnologylab.project.Uebung2.data;

import org.texttechnologylab.project.Uebung2.data.Comment;
import org.texttechnologylab.project.Uebung2.data.Fraction;
import org.texttechnologylab.project.Uebung2.data.Party;
import org.texttechnologylab.project.Uebung2.data.PlenaryObject;

import java.util.Set;

public interface Speaker extends PlenaryObject {
    Party getParty();

    void setParty(Party pParty);

    Fraction getFraction();

    void setFraction(Fraction pFraction);

    String getRole();

    void setRole(String sValue);

    String getTitle();

    void setTitle(String sValue);

    String getName();

    void setName(String sValue);

    String getFirstName();

    void setFirstName(String sValue);

    Set<Speech> getSpeeches();

    void addSpeech(Speech pSpeech);

    void addSpeeches(Set<Speech> pSet);

    Set<Comment> getComments();

    float getAvgLength();

    @Override
    String toString();

    boolean isLeader();
}
