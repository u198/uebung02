package org.texttechnologylab.project.Uebung2.data.database;

import java.io.*;
import java.util.Properties;

public class MongoDBConfig extends Properties {

    public MongoDBConfig(File pFile) throws IOException {
        this(pFile.getAbsolutePath());
    }

    public MongoDBConfig(String path) throws IOException {
        try {
        BufferedReader lReader = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
        this.load(lReader);
        lReader.close();
        } catch (IOException e){
        e.printStackTrace();
        }
    }

    public String getMongoHostName(){
        return getProperty("remote_host", "127.0.0.1");
    }

    public String getMongoUsername(){
        return getProperty("remote_user", "user");
    }

    public String getMongoPassword(){
        return getProperty("remote_password", "password");
    }

    public String getMongoDatabase(){
        return getProperty("remote_database", "database");
    }

    public int getMongoPort(){
        return Integer.valueOf(getProperty("remote_port", "27017"));
    }

    public String getMongoCollection(){
        return getProperty("remote_collection", "collection");
    }
    public String getMongoCollectionP(){
        return getProperty("remote_collectionPartei", "collection");
    }
}
