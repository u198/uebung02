package org.texttechnologylab.project.Uebung2.data;

import org.texttechnologylab.project.Uebung2.data.Fraction;
import org.w3c.dom.Node;

import java.util.Set;

public interface ParliamentFactory {
    Set<Speaker> getSpeakers();

    Set<PlenaryProtocol> getProtocols();

    void addProtocol(PlenaryProtocol pProtocol);

    Set<Fraction> getFractions();

    Set<Party> getParties();

    Party getParty(String sName);

    Speaker getSpeaker(String sId);

    Speaker getSpeakerByName(String sValue);

    Speaker getSpeaker(Node pNode);

    Fraction getFraction(String sName);

    Fraction getFraction(Node pNode);
}
