package org.texttechnologylab.project.Uebung2.data;

import org.texttechnologylab.project.Uebung2.data.AgendaItem;
import org.texttechnologylab.project.Uebung2.data.Fraction;
import org.texttechnologylab.project.Uebung2.data.Party;
import org.texttechnologylab.project.Uebung2.data.PlenaryObject;

import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Set;

public interface PlenaryProtocol extends PlenaryObject {
    int getIndex();

    void setIndex(int iValue);

    Date getDate();

    void setDate(Date pDate);

    Time getStartTime();

    void setStarttime(Time pTime);

    Time getEndTime();

    void setEndTime(Time pTime);

    String getTitle();

    void setTitle(String sValue);

    List<AgendaItem> getAgendaItems();

    void addAgendaItem(AgendaItem pItem);

    void addAgendaItems(Set<AgendaItem> pSet);

    String getPlace();

    void setPlace(String sValue);

    Set<Speaker> getSpeakers();

    Set<Speaker> getSpeakers(Party pParty);

    Set<Speaker> getSpeakers(Fraction pFraction);

    Set<Speaker> getLeaders();

    @Override
    String toString();

    @Override
    boolean equals(Object o);

    @Override
    int hashCode();

    @Override
    int compareTo(PlenaryObject plenaryObject);
}
