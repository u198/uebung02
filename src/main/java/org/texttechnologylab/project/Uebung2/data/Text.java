package org.texttechnologylab.project.Uebung2.data;

import org.texttechnologylab.project.Uebung2.data.Speaker;
import org.texttechnologylab.project.Uebung2.data.Speech;

public interface Text {
    Speech getSpeech();

    Speaker getSpeaker();

    void setSpeech(Speech pSpeech);

    void setSpeaker(Speaker pSpeaker);

    String getContent();
}
