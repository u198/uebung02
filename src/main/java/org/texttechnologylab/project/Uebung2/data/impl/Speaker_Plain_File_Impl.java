package org.texttechnologylab.project.Uebung2.data.impl;

import org.texttechnologylab.project.Uebung2.data.ParliamentFactory;

public class Speaker_Plain_File_Impl extends Speaker_File_Impl {

    /**
     * Constructor
     * @param pFactory
     */
    public Speaker_Plain_File_Impl(ParliamentFactory pFactory) {
        super(pFactory);
    }

    @Override
    public void setName(String sValue) {
        super.setName(transform(sValue));
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public boolean isLeader() {
        return super.isLeader();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    public static String transform(String sValue){

        String sReturn = sValue;

        sReturn = sReturn.replaceAll("Vizepräsident in", "Vizepräsidentin");
        sReturn = sReturn.replaceAll("Vizepräsiden t", "Vizepräsident");
        sReturn = sReturn.replaceAll(":", "");

        return sReturn;

    }

}