package org.texttechnologylab.project.Uebung2.data;

import java.util.List;
import java.util.Set;

public interface AgendaItem {
    List<Speech> getSpeeches();

    void addSpeech(Speech pValue);

    void addSpeeches(Set<Speech> pSet);

    String getIndex();

    void setIndex(String sValue);

    String getTitle();

    void setTitle(String sValue);

    PlenaryProtocol getProtocol();

    @Override
    String toString();

    @Override
    boolean equals(Object o);

    @Override
    int hashCode();
}
