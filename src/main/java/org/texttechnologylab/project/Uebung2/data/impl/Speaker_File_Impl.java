package org.texttechnologylab.project.Uebung2.data.impl;

import org.texttechnologylab.project.Uebung2.data.*;
import org.texttechnologylab.project.Uebung2.data.database.MongoDoc;
import org.texttechnologylab.project.Uebung2.helper.XMLNodes;
import org.w3c.dom.Node;

import java.util.HashSet;
import java.util.Set;

public class Speaker_File_Impl extends PlenaryObject_File_Impl implements Speaker {

    // Speaker variables
    protected String sName = "";
    protected String sFirstName = "";
    protected String sTitle = "";
    protected String sRole = "";

    protected Set<Speech> pSpeeches = new HashSet<>();

    protected Fraction pFraction = null;
    protected Party pParty = null;

    /**
     * Constructor
     * @param pFactory
     */
    public Speaker_File_Impl(ParliamentFactory pFactory){
        super(pFactory);
    }


    /**
     * Constructor
     * @param pFactory
     * @param pNode
     */
    public Speaker_File_Impl(ParliamentFactory pFactory, Node pNode){
        super(pFactory);
        this.setID(pNode.getAttributes().getNamedItem("id").getTextContent());
        init(pNode);
    }

    /**
     * Constructor
     * @param pSpeech
     * @param pNode
     */
    public Speaker_File_Impl(Speech pSpeech, Node pNode){
        super(pSpeech.getFactory());
        this.setID(pNode.getAttributes().getNamedItem("id").getTextContent());

        this.addSpeech(pSpeech);

        init(pNode);

    }

    /**
     * Internal init method based on XML-node
     * @param pNode
     */
    private void init(Node pNode){

        Node nSurname = XMLNodes.getOneNode(pNode, "nachname");
        Node nFirstName = XMLNodes.getOneNode(pNode, "vorname");
        Node nNamensZusatz = XMLNodes.getOneNode(pNode, "namenszusatz");
        Node nTitle = XMLNodes.getOneNode(pNode, "titel");
        Node nRole = XMLNodes.getOneNode(pNode, "rolle_lang");
        Node nFraction = XMLNodes.getOneNode(pNode, "fraktion");

        if(nSurname!=null){
            this.setName(nSurname.getTextContent());
        }
        if(nNamensZusatz!=null){
            this.setName(nNamensZusatz.getTextContent()+" "+this.getName());
        }
        if(nFirstName!=null){
            this.setFirstName(nFirstName.getTextContent());
        }
        if(nTitle!=null){
            this.setTitle(nTitle.getTextContent());
        }
        if(nRole!=null){
            this.setRole(nRole.getTextContent());
        }
        if(nFraction!=null){
            this.setFraction(this.getFactory().getFraction(nFraction));
            this.getFraction().addMember(this);
        }



    }

    @Override
    public Party getParty() {
        return this.pParty;
    }

    @Override
    public void setParty(Party pParty) {
        this.pParty = pParty;
        pParty.addMember(this);
    }

    @Override
    public Fraction getFraction() {
        return this.pFraction;
    }

    @Override
    public void setFraction(Fraction pFraction) {
        this.pFraction = pFraction;
        pFraction.addMember(this);
    }

    @Override
    public String getRole() {
        return this.sRole;
    }

    @Override
    public void setRole(String sValue) {
        this.sRole = sValue;
    }

    @Override
    public String getTitle() {
        return this.sTitle;
    }

    @Override
    public void setTitle(String sValue) {
        this.sTitle = sValue;
    }

    @Override
    public String getName() {
        return this.sName;
    }

    @Override
    public void setName(String sValue) {
        this.sName = sValue;
    }

    @Override
    public String getFirstName() {
        return this.sFirstName;
    }

    @Override
    public void setFirstName(String sValue) {
        this.sFirstName = sValue;
    }

    @Override
    public Set<Speech> getSpeeches() {
        return this.pSpeeches;
    }

    @Override
    public void addSpeech(Speech pSpeech) {
        this.pSpeeches.add(pSpeech);
    }

    @Override
    public void addSpeeches(Set<Speech> pSet) {
        pSet.forEach(s->{
            addSpeech(s);
        });
    }

    @Override
    public Set<Comment> getComments() {
        Set<Comment> rSet = new HashSet<>(0);
        this.getSpeeches().stream().forEach(speech -> {
            rSet.addAll(speech.getComments());
        });
        return rSet;
    }


    @Override
    public float getAvgLength() {
        float rFloat = 0.f;

        int iSum = this.getSpeeches().stream().mapToInt(s->s.getLength()).sum();

        rFloat = iSum / this.getSpeeches().size();

        return rFloat;
    }

    @Override
    public String toString() {
        return this.getTitle()+"\t"+this.getFirstName()+"\t"+this.getName()+"\t"+(this.getRole().length()>0 ? ", "+this.getRole() : ""+"\t"+(this.getFraction()!=null ? "F: ("+this.getFraction()+")" : "")+"\t"+this.getParty()!=null ? "P: "+this.getParty() : "");
    }

    @Override
    public boolean isLeader(){
        boolean rBool = false;

//            if(this instanceof Speaker_Plain_File_Impl){
//                System.out.println("Stop");
//            };

        rBool = this.getRole().startsWith("Präsident") || this.getRole().startsWith("Vizepräsident") || this.getRole().toLowerCase().startsWith("Alters");

        if (!rBool) {
            rBool = this.getName().startsWith("Präsident") || this.getName().startsWith("Vizepräsident");
        }

        return rBool;
    }
}