package org.texttechnologylab.project.Uebung2.data.impl;

import org.texttechnologylab.project.Uebung2.data.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PlenaryProtocol_File_Impl extends PlenaryObject_File_Impl implements PlenaryProtocol {

    // The DOM document is stored
    private Document pDocument = null;

    // variable declaration
    private int iIndex = -1;
    private Date pDate = null;
    private Time pStartTime = null;
    private Time pEndTime = null;
    private String sTitle = "";
    private String sPlace = "";

    private List<AgendaItem> pAgendaItems = new ArrayList<>(0);

    /**
     * Constuctor. The constructor needs a ParliamentFactory and the file of the plenary protocol
     * @param pFactory
     * @param pFile
     */
    public PlenaryProtocol_File_Impl(ParliamentFactory pFactory, File pFile){
        super(pFactory);
        try {
            init(pFile);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialization based on a file
     * @param pFile
     */
    private void init(File pFile) throws ParserConfigurationException, IOException, SAXException, ParseException {

        // Define Date and Time Format
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

        // create a document builder factory for the parsing of XML documents
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // parse XML file
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document pDocument = db.parse(pFile);
        this.pDocument=pDocument;

        /**
         * Extract information of the header of the file, by use of a helper method
         */
        Node nWahlperiode = getNodeFromXML(pDocument, "wahlperiode");
        this.setWahlperiode(Integer.valueOf(nWahlperiode.getTextContent()));

        Node nSitzungsnummer = getNodeFromXML(pDocument, "sitzungsnr");
        this.setIndex(Integer.valueOf(nSitzungsnummer.getTextContent()));

        Node nTitle = getNodeFromXML(pDocument, "plenarprotokoll-nummer");
        this.setTitle(nTitle.getTextContent());

        Node nOrt = getNodeFromXML(pDocument, "ort");
        this.setPlace(nOrt.getTextContent());

        Node nDatum = getNodeFromXML(pDocument, "datum");
        Date pDate = new Date(sdfDate.parse(nDatum.getAttributes().getNamedItem("date").getTextContent()).getTime());
        this.setDate(pDate);

        // extract the start of a session
        Node nStart = getNodeFromXML(pDocument, "sitzungsbeginn");
        Time pStartTime = null;
        try {
            pStartTime = new Time(sdfTime.parse(nStart.getAttributes().getNamedItem("sitzung-start-uhrzeit").getTextContent()).getTime());
        }
        catch (ParseException pe){
            pStartTime = new Time(sdfTime.parse(nStart.getAttributes().getNamedItem("sitzung-start-uhrzeit").getTextContent().replaceAll("\\.", ":")).getTime());
        }
        this.setStarttime(pStartTime);

        // extract the start of a session
        Node nEnde = getNodeFromXML(pDocument, "sitzungsende");

        Time pEndTime = null;
        try {
            pEndTime = new Time(sdfTime.parse(nEnde.getAttributes().getNamedItem("sitzung-ende-uhrzeit").getTextContent()).getTime());
        }
        catch (ParseException pe){
            pStartTime = new Time(sdfTime.parse(nEnde.getAttributes().getNamedItem("sitzung-ende-uhrzeit").getTextContent().replaceAll("\\.", ":")).getTime());
        }
        this.setEndTime(pEndTime);

        // Processing AgendaIgems
        NodeList pBlocks = pDocument.getElementsByTagName("ivz-block");

        for(int b=0; b<pBlocks.getLength(); b++){
            Node n = pBlocks.item(b);

            AgendaItem pItem = new AgendaItem_File_Impl(this, n);
            if(pItem.getSpeeches().size()>0){
                this.addAgendaItem(pItem);
            }

        }

    }

    /**
     * Return the XML-document
     * @return
     */
    protected Document getFile(){
        return this.pDocument;
    }

    /**
     * Extract a Node by a tag-name
     * @param pDocument
     * @param sTag
     * @return
     */
    private Node getNodeFromXML(Document pDocument, String sTag){
        return pDocument.getElementsByTagName(sTag).item(0);
    }

    @Override
    public int getIndex() {
        return this.iIndex;
    }

    @Override
    public void setIndex(int iValue) {
        this.iIndex = iValue;
    }

    @Override
    public Date getDate() {
        return this.pDate;
    }

    @Override
    public void setDate(Date pDate) {
        this.pDate = pDate;
    }

    @Override
    public Time getStartTime() {
        return this.pStartTime;
    }

    @Override
    public void setStarttime(Time pTime) {
        this.pStartTime = pTime;
    }

    @Override
    public Time getEndTime() {
        return this.pEndTime;
    }

    @Override
    public void setEndTime(Time pTime) {
        this.pEndTime = pTime;
    }

    @Override
    public String getTitle() {
        return this.sTitle;
    }

    @Override
    public void setTitle(String sValue) {
        this.sTitle = sValue;
    }

    @Override
    public List<AgendaItem> getAgendaItems() {
        return this.pAgendaItems;
    }

    @Override
    public void addAgendaItem(AgendaItem pItem) {
        this.pAgendaItems.add(pItem);
    }

    @Override
    public void addAgendaItems(Set<AgendaItem> pSet) {
        pSet.forEach(i->{
            this.addAgendaItem(i);
        });
    }

    @Override
    public String getPlace() {
        return this.sPlace;
    }

    @Override
    public void setPlace(String sValue) {
        this.sPlace = sValue;
    }

    @Override
    public Set<Speaker> getSpeakers() {
        Set<Speaker> rSet = new HashSet<>(0);

        getAgendaItems().forEach(ai->{
            ai.getSpeeches().forEach(speech -> {
                rSet.add(speech.getSpeaker());
            });
        });

        return rSet;
    }

    @Override
    public Set<Speaker> getSpeakers(Party pParty) {
        return getSpeakers().stream().filter(s->s.getParty().equals(pParty)).collect(Collectors.toSet());
    }

    @Override
    public Set<Speaker> getSpeakers(Fraction pFraction) {
        return getSpeakers().stream().filter(s->s.getFraction().equals(pFraction)).collect(Collectors.toSet());
    }

    @Override
    public Set<Speaker> getLeaders() {

        /*
         * Search all agenda items, in their speech, who interrupted a speech and if they are a president.
         */
        Set<Speaker> rSet = new HashSet<>(0);
        this.getAgendaItems().forEach(ai->{
            ai.getSpeeches().forEach(s->{
                s.getInsertions().forEach(i->{
                    if(i.getSpeaker().isLeader()){
                        rSet.add(i.getSpeaker());
                    }
                });
            });
        });

        return rSet;
    }

    @Override
    public String toString() {
        return this.getIndex()+"\t"+this.getDate()+"\t"+this.getPlace();
    }

    @Override
    public boolean equals(Object o) {
        return this.hashCode()==o.hashCode();
    }

    /**
     * Special hashCode method
     * @return
     */
    @Override
    public int hashCode() {
        return this.getIndex();
    }

    /**
     * Special compareTo method
     * @param plenaryObject
     * @return
     */
    @Override
    public int compareTo(PlenaryObject plenaryObject) {
        if(plenaryObject instanceof PlenaryProtocol){
            return ((PlenaryProtocol)plenaryObject).getDate().compareTo(this.getDate());
        }
        return super.compareTo(plenaryObject);
    }

}
