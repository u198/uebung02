package org.texttechnologylab.project.Uebung2.data;

import java.util.Set;

public interface Party extends Comparable<Party> {
    String getName();

    void setName(String sValue);

    Set<Speaker> getMembers();

    void addMember(Speaker pMember);

    void addMembers(Set<Speaker> pSet);

    int compareTo(Party party);
}
