package org.texttechnologylab.project.Uebung2.helper;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class Reader {

    public static Set<File> getFiles(String sPath, String sSuffix){

        Set<File> fSet = new HashSet<>(0);

        File sDir = new File(sPath);

        if(sDir.isDirectory()){
            for (File f : sDir.listFiles()) {
                if (f.isFile() && f.getName().endsWith(".xml")) {
                    fSet.add(f);
                }

            }
        }
        else if(sDir.isFile()){
            if(sDir.getName().endsWith(sSuffix)) {
                fSet.add(sDir);
            }
        }

        return fSet;

    }

}
