package org.texttechnologylab.project.Uebung2.helper;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class XMLNodes {

    public static List<Node> getNodes(Node node, String name) {

        List<Node> set = new ArrayList<>(0);

        if(node.getNodeName().equals(name)) {
            set.add(node);
        }
        else {
            if (node.hasChildNodes()) {
                for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                    set.addAll(getNodes(node.getChildNodes().item(i), name));
                }
            } else {
                if (node.getNodeName().equals(name)) {
                    set.add(node);
                }
            }
        }
        return set;
    }
    public static Node getOneNode(Node node, String name) {
        List <Node> nList = getNodes(node, name);
        if (nList.size()>0) {
            return nList.stream().findFirst().get();
        }
        return null;
    }
}
