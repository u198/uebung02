# Uebung 02

## zu finden auf: (https://gitlab.com/u198/uebung02)

## Main Methode
Beim starten des Programms muss man als Parameter -db C:\Beispiel\Folder\DBConfig.txt übergeben (DBConfig.txt ist bei der Abgabe enthalten)

Man wird kurz darauf aufgefordert den Path zu den XML-Dateien anzugeben zum Beispiel: C:\Beispiel\Folder

### Funktionen

Das Programm kann leider nur alle Redner in Dokumente umwandeln und in die Collection packen oder alle Redner geordnet nach der Partei in die Collection packen und ausgeben.